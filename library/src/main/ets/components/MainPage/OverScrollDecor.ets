/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


@ComponentV2
struct OverScrollDecor {
  @Param @Require model: OverScrollDecor.Model
  @Event $model: (value: OverScrollDecor.Model) => void = (value: OverScrollDecor.Model) => {};
  @BuilderParam child?: () => void

  private scroller: Scroller = new Scroller()
  private endOffsetY = 0
  private endOffsetX = 0
  private isBounceEffect = true

  build() {
    if (this.model.mUpOverScroll) {
      if (this.model.mOrientation == OverScrollDecor.ORIENTATION.VERTICAL) {
        Flex({ alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
          Scroll(this.scroller) {
            if( this.child !== undefined){
              this.child()
            }
          }
          .margin(this.model.mMargin)
          .scrollable(ScrollDirection.Vertical)
          .scrollBar(this.model.mScrollBar ? BarState.Auto : BarState.Off)
          .edgeEffect(this.model.mOverScrollBounceEffect ? EdgeEffect.Spring : EdgeEffect.None)
          .onScrollEdge((side: Edge) => {
            if (side == Edge.Bottom) {
              this.endOffsetY = this.scroller.currentOffset().yOffset
            }
          })
          .onDidScroll(() => {
            this.model.mOffsetY = Math.floor(this.scroller.currentOffset().yOffset)
            if (this.model.mOffsetY < 0) {
              this.model.mOffsetY = -Math.floor(this.model.mOffsetY)
              if (this.isBounceEffect) {
                this.model.mTextColor = this.model.mDragColorTop
              }else {
                this.model.mTextColor = this.model.mBounceBackColorTop
              }
              if (this.model.mOffsetY == 1) {
                this.scroller.scrollTo({xOffset: 0, yOffset: 0})
              }
            }else if (this.model.mOffsetY > this.endOffsetY && this.endOffsetY != 0) {
              this.model.mOffsetY = Math.floor(this.endOffsetY - this.model.mOffsetY)
              if (this.isBounceEffect) {
                this.model.mTextColor = this.model.mDragColorBottom
              }else {
                this.model.mTextColor = this.model.mBounceBackColorBottom
              }
            } else {
              this.model.mOffsetY = 0
              this.model.mTextColor = this.model.mClearColor
            }
          })
          .onTouch((event?: TouchEvent) => {
            if (event == undefined){
              return;
            }
            if (event.type == TouchType.Down) {
              this.isBounceEffect = true
            }
            if (event.type == TouchType.Up) {
              this.isBounceEffect = false
            }
          })
        }.height(this.model.mHeight)
        .width(this.model.mWidth)
      } else if (this.model.mOrientation == OverScrollDecor.ORIENTATION.HORIZONTAL) {
        Flex({ alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
          Scroll(this.scroller) {
            if (this.child !== undefined) {
              this.child()
            }
          }
          .margin(this.model.mMargin)
          .scrollable(ScrollDirection.Horizontal)
          .scrollBar(this.model.mScrollBar ? BarState.Auto : BarState.Off)
          .edgeEffect(this.model.mOverScrollBounceEffect ? EdgeEffect.Spring : EdgeEffect.None)
          .onScrollEdge((side: Edge) => {
            if (side == Edge.Baseline) {
              this.endOffsetX = this.scroller.currentOffset().xOffset
            }
          })
          .onDidScroll(() => {
            this.model.mOffsetX = Math.floor(this.scroller.currentOffset().xOffset)
            if (this.model.mOffsetX < 0) {
              this.model.mOffsetX = -Math.floor(this.model.mOffsetX)
              if (this.isBounceEffect) {
                this.model.mTextColor = this.model.mDragColorLeft
              }else {
                this.model.mTextColor = this.model.mBounceBackColorLeft
              }
              if (this.model.mOffsetX == 1) {
                this.scroller.scrollTo({xOffset: 0, yOffset: 0})
              }
            }else if (this.model.mOffsetX > this.endOffsetX && this.endOffsetX != 0) {
              this.model.mOffsetX = Math.floor(this.endOffsetX - this.model.mOffsetX)
              if (this.isBounceEffect) {
                this.model.mTextColor = this.model.mDragColorRight
              }else {
                this.model.mTextColor = this.model.mBounceBackColorRight
              }
            } else {
              this.model.mOffsetX = 0
              this.model.mTextColor = this.model.mClearColor
            }
          })
          .onTouch((event?: TouchEvent) => {
            if (event === undefined) {
              return;
            }
            if (event.type === TouchType.Down) {
              this.isBounceEffect = true
            }
            if (event.type === TouchType.Up) {
              this.isBounceEffect = false
            }
          })
        }.height(this.model.mHeight)
        .width(this.model.mWidth)
      }
    } else {
      Flex({ alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
        Scroll(this.scroller) {
          if (this.child !== undefined) {
            this.child()
          }
        }
        .margin(this.model.mMargin)
        .scrollable(ScrollDirection.None)
      }.height(this.model.mHeight)
      .width(this.model.mWidth)
    }
  }
}

namespace OverScrollDecor {
  export enum ORIENTATION {
    VERTICAL = 0,
    HORIZONTAL = 1
  }

  @ObservedV2
  export class Model {
    @Trace mHeight: number | string = px2vp(2340)
    @Trace mWidth: number | string = px2vp(lpx2px(720))
    @Trace mMargin: number = 16
    //设置滚动方向
    @Trace mOrientation:  ORIENTATION = 0
    //是否需要滚动条
    @Trace mScrollBar: boolean = true
    //是否需要滑动效果
    @Trace mOverScrollBounceEffect: boolean = false
    //是否可以滑动
    @Trace mUpOverScroll: boolean = true
    @Trace mOffsetY: number = 0
    @Trace mOffsetX: number = 0
    @Trace mTextColor: string = "#727171"
    @Trace mClearColor: string = "#727171"
    @Trace mDragColorLeft: string = "#ffaa66cc"
    @Trace mBounceBackColorLeft: string = "#ff33b5e5"
    @Trace mDragColorRight: string = "#ffff4444"
    @Trace mBounceBackColorRight: string = "#ffff8800"
    @Trace mDragColorTop: string = "#ffff4444"
    @Trace mBounceBackColorTop: string = "#ffff8800"
    @Trace mDragColorBottom: string = "#ffaa66cc"
    @Trace mBounceBackColorBottom: string = "#ff33b5e5"

    public getHeight(): number | string {
      return this.mHeight
    }

    public setHeight(height: number | string): Model {
      this.mHeight = height
      return this
    }

    public getWidth(): number | string {
      return this.mWidth
    }

    public setWidth(width: number | string): Model {
      this.mWidth = width
      return this
    }

    public getMargin(): number {
      return this.mMargin
    }

    public setMargin(margin: number): Model {
      this.mMargin = margin
      return this
    }

    public getOrientation(): number {
      return this.mOrientation
    }

    public setOrientation(orientation: number): Model {
      this.mOrientation = orientation
      return this
    }

    public isScrollBar(): boolean {
      return this.mScrollBar
    }

    public setScrollBar(scrollBar: boolean): Model {
      this.mScrollBar = scrollBar
      return this
    }

    public isOverScrollBounceEffect(): boolean {
      return this.mOverScrollBounceEffect
    }

    public setOverScrollBounceEffect(overScrollBounceEffect: boolean): Model {
      this.mOverScrollBounceEffect = overScrollBounceEffect
      return this
    }

    public isUpOverScroll(): boolean {
      return this.mUpOverScroll
    }

    public setUpOverScroll(upOverScroll: boolean): Model {
      this.mUpOverScroll = upOverScroll
      return this
    }

    public getOffsetX(): number {
      return this.mOffsetX
    }

    public setOffsetX(offsetX: number): Model {
      this.mOffsetX = offsetX
      return this
    }

    public getOffsetY(): number {
      return this.mOffsetY
    }

    public setOffsetY(offsetY: number): Model {
      this.mOffsetY = offsetY
      return this
    }

    public getTextColor(): string {
      return this.mTextColor
    }

    public setTextColor(textColor: string): Model {
      this.mTextColor = textColor
      return this
    }

    public getClearColor(): string {
      return this.mClearColor
    }

    public setClearColor(clearColor: string): Model {
      this.mClearColor = clearColor
      return this
    }

    public getDragColorLeft(): string {
      return this.mDragColorLeft
    }

    public setDragColorLeft(dragColorLeft: string): Model {
      this.mDragColorLeft = dragColorLeft
      return this
    }

    public getBounceBackColorLeft(): string {
      return this.mBounceBackColorLeft
    }

    public setBounceBackColorLeft(bounceBackColorLeft: string): Model {
      this.mBounceBackColorLeft = bounceBackColorLeft
      return this
    }

    public getDragColorRight(): string {
      return this.mDragColorRight
    }

    public setDragColorRight(dragColorRight: string): Model {
      this.mDragColorRight = dragColorRight
      return this
    }

    public getBounceBackColorRight(): string {
      return this.mBounceBackColorRight
    }

    public setBounceBackColorRight(bounceBackColorRight: string): Model {
      this.mBounceBackColorRight = bounceBackColorRight
      return this
    }

    public getDragColorTop(): string {
      return this.mDragColorTop
    }

    public setDragColorTop(dragColorTop: string): Model {
      this.mDragColorTop = dragColorTop
      return this
    }

    public getBounceBackColorTop(): string {
      return this.mBounceBackColorTop
    }

    public setBounceBackColorTop(bounceBackColorTop: string): Model {
      this.mBounceBackColorTop = bounceBackColorTop
      return this
    }

    public getDragColorBottom(): string {
      return this.mDragColorBottom
    }

    public setDragColorBottom(dragColorBottom: string): Model {
      this.mDragColorBottom = dragColorBottom
      return this
    }

    public getBounceBackColorBottom(): string {
      return this.mBounceBackColorBottom
    }

    public setBounceBackColorBottom(bounceBackColorBottom: string): Model {
      this.mBounceBackColorBottom = bounceBackColorBottom
      return this
    }
  }
}

export default OverScrollDecor;


